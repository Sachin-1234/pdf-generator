process.env.NODE_ENV = 'test';
//const path = require('path');
import * as path from 'path'
//require('app-module-path').addPath(path.join(__dirname, '../'));
import * as app_module_path from 'app-module-path'
//const chai = require('chai');
import * as chai from 'chai'
//const chaiHttp = require('chai-http');
//const should = chai.should();

//const request = require('supertest');
import request from 'supertest'
//const server = require('../server');
import * as server from '../server'
//const mongo = require('../services/databases/mongodb');
import * as mongo from "../configurations/mongo-connection"
import { ConfigurationSettingsInterface } from '../config-interfaces/ConfigurationSettingsInterface'
import { MongoConfigurationInterface } from '../config-interfaces/MongoConfigurationInterface'
import * as Configs from '../configurations'
/** ***************************** END IMPORT LIBRARIES ****************************** */

/** ***************************** START TEST FUNCTIONS ****************************** */
app_module_path.addPath(path.join(__dirname, '../'));
const expect = chai.expect;
const allConfigurations: ConfigurationSettingsInterface = Configs.getConfigurations();

let mongoConfig: MongoConfigurationInterface = allConfigurations.mongoConfig;

describe('templates', () => {
    let result;
    let mongo_result; let
        _id;
    before(async () => {
        // Before block
        try {
            result = await server.init();
            mongo_result = await mongo.init(mongoConfig);
            console.log('Server Started :');
            return result;
        } catch (e) {
            console.log('Error in starting server : ', e);
            return e;
        }
    });

    describe('POST /register', () => {
        it('it should registers the template', (done) => {
            const options = {
                method: 'POST',
                url: '/register',
                headers: {
                    "authorization": "Basic token"
                }
            };
            request(allConfigurations.serverConfig.host).post(options.url)
                .set('authorization', 'Basic token')
                .field('Content-Type', 'multipart/form-data')
                .field('placeholder_json', {
                    "invoiceBox":{
                         "top":{
                             "Invoice":" 1234",
                             "Created": " february 1,2020",
                             "Due":" April 1, 2021"
                          },
                         "information":[
                            {
                              "address":"Sparksuite, Inc. 12345 Sunny Road Sunnyville, CA 123456"
                            },
                            {
                              "address":"Acme Corp. John Doe john@example.com"
                            }
                          ],
                          "heading":[
                              "Payment Method",
                              "Check #",
                              "Item",
                              "Price"
                          ],
                          "details":[
                              "Check",
                              "2000"
                          ],
                          "item":[
                              "Website development",
                              "$600.00",
                              "Hosting (5 months)",
                              "$75.00"
                          ],
                          "item_last":[
                              "Domain name (1 year)",
                              "$10.00"
                          ],
                          "total":{
                              "Total":" $385.00"
                           }
                         
                    }
            })
                .attach('template_file', '/home/iauro/html/invoice_template.html')
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        done();
                    }
                });
        });
    });

    /* describe('POST /generate-pdf/:id', () => {
        it('it should generate the pdf file', (done) => {
            const options = {
                method: 'POST',
                url: '/generate-pdf',
                headers: {
                    "authorization": "Basic token"
                }
            };
            request(allConfigurations.serverConfig.host).post(options.url)
                .set('authorization', 'Basic token')
                .send({
                    "invoiceBox":{
                         "top":{
                             "Invoice":" 1234",
                             "Created": " february 1,2020",
                             "Due":" April 1, 2021"
                          },
                         "information":[
                            {
                              "address":"Sparksuite, Inc. 12345 Sunny Road Sunnyville, CA 123456"
                            },
                            {
                              "address":"Acme Corp. John Doe john@example.com"
                            }
                          ],
                          "heading":[
                              "Payment Method",
                              "Check #",
                              "Item",
                              "Price"
                          ],
                          "details":[
                              "Check",
                              "2000"
                          ],
                          "item":[
                              "Website development",
                              "$600.00",
                              "Hosting (5 months)",
                              "$75.00"
                          ],
                          "item_last":[
                              "Domain name (1 year)",
                              "$10.00"
                          ],
                          "total":{
                              "Total":" $385.00"
                           }
                         
                    }
            })
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.should.have.status(200);
                        done();
                    }
                });
        });
    });
 */

    after(async () => {
        // Before block
        try {
            const mongo_close = await mongo.close();
            //result.close();
        } catch (e) {
            console.log('some error occured: ', e);
            return e;
        }
    });
});