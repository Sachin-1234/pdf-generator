
import { MongoConfigurationInterface } from '../config-interfaces/MongoConfigurationInterface';
import * as Mongoose from "mongoose";
import { TemplatesModel } from '../components/templates/models/templates'

export interface DatabaseModelsInterface {

    TemplatesModel: Mongoose.Model<any>;

}

export function init(config: MongoConfigurationInterface): DatabaseModelsInterface {

    console.log(config.connectionString);

    (<any>Mongoose).Promise = Promise; Mongoose.connect(config.connectionString, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true });

    Mongoose.set('debug', function (coll: any, method: any, query: any, doc: any) {

        console.log(coll);
        console.log(method);
        console.log("query", query);
        console.log(doc);
    });

    let mongoDb = Mongoose.connection;

    mongoDb.on('error', () => {
        console.log(`Unable to connect to database: ${config.connectionString}`);
    });

    mongoDb.once('open', () => {
        console.log(`Connected to database: ${config.connectionString}`);
    });

    process.on('SIGINT', () => {
        Mongoose.connection.close(() => {
            console.log('Mongoose default connection disconnected through app termination');
            process.exit(0);
        });
    });

    return {
        TemplatesModel: TemplatesModel
    };
}

export async function close() {
    return await Mongoose.connection.close();
};