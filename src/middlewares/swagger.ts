
import * as Configs from '../configurations'
import { ConfigurationSettingsInterface } from '../config-interfaces/ConfigurationSettingsInterface'
import { SwaggerConfigurationInterface } from '../config-interfaces/SwaggerConfigurationInterface'
import { object } from 'joi';

const allCofigurations: ConfigurationSettingsInterface = Configs.getConfigurations();
let swaggerConfig: SwaggerConfigurationInterface = allCofigurations.swaggerConfig;

export const Doc = {
    swagger: '2.0',
    title: 'pdf-generator',
    info: {
        version: '1.0.0',
        title: 'pdf-generator',
        description: 'pdf-generator Api Documentation.',
    },
    tags: [
        {
            name: 'Templates',
            description: 'This service can generate pdf files using invoice templates'
        }
    ],
    host: swaggerConfig.host,
    basePath: swaggerConfig.swaggerBasePath,
    schemes: ['http', 'https'],
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            },
        },
    },
    security: [
        {
            bearerAuth: [],
        },
    ],
    paths: {
        '/register': {
            post: {
                consumes:"multipart/form-data",
                tags: ['Templates'],
                summary: 'registers new template',
                description: 'registers new template',
                parameters: [
                    {
                        name: 'authorization',
                        in: 'header',
                        type: 'string',
                        required: true,
                    },
                    {
                        in: 'formData',
                        required: false,
                        name: 'template_file',
                        type: 'file'
                    },
                    {
                        in: 'formData',
                        required: false,
                        name: 'placeholder_json',
                        type: 'object'
                    }
                ],
                responses: {
                    '200': {
                        description: 'Successful',
                        schema: {
                            type: 'string'
                        }
                    },
                    
                    '400': {
                        description: 'Bad request',
                        schema: {
                            type: 'object'
                        }
                        
                    },
                    '500': {
                        description: 'Internal Server Error',
                        schema: {
                            type: 'object'
                        }
                    },
                },
            },
        },
        '/generate-pdf/{id}': {
            post: {
                consumes:"application/json",
                tags: ['Templates'],
                summary: 'generate pdf for template id',
                description: 'generate pdf for template id',
                parameters: [
                    {
                        name: 'authorization',
                        in: 'header',
                        type: 'string',
                        required: true,
                    },
                    {
                        in: 'body',
                        required: true,
                        name: 'placeholder_json',
                        type: 'object'
                    },
                    {
                        in: 'path',
                        name: 'id',
                        type: 'string',
                        required: true
                    },
                    {
                        in: 'query',
                        name: 'margin',
                        type: 'object',
                        required: true
                    }
                ],
                responses: {
                    '200': {
                        description: 'A pdf file',
                        content: {
                            'application/pdf': {
                                    schema:{
                                        type: 'string',
                                        format: 'binary'
                                    }
                            }
                        }
                    },
                    
                    '400': {
                        description: 'Bad request',
                        schema: {
                            type: 'object'
                        }
                    },
                    '500': {
                        description: 'Internal Server Error',
                        schema: {
                            type: 'object'
                        }
                    },
                },
            },
        }
    },
    definitions: {
        Templates: {
            type: 'object',
            properties: {
                id: {
                    type: 'string',
                    description: 'the auto generated id for templates'
                },
                template_file: {
                    type: 'file',
                    description: 'the invoice template html file'
                },
                placeholder_json: {
                    type: 'file',
                    description: 'the placeholder json'
                },

            }
        },
        Generated_Files: {
            type: 'object',
            properties: {
                file_path: {
                    type: 'string',
                    description: 'file path of generated pdf file'
                },
                template_id: {
                    type: 'string',
                    description: 'template id from which pdf is generated'
                },
                placeholder_json_file: {
                    type: 'string',
                    description: 'input placeholder json'
                }
            }
        }
    }
};
