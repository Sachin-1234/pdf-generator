/* eslint-disable import/no-unresolved */
import * as express from 'express'
import * as Configs from './configurations'
import * as helmet from 'helmet'
import * as cluster from 'cluster'
//import log from './services/logger/logger'
import component from './components/index'
import * as os from 'os'
import * as swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './middlewares/swagger';
import { ConfigurationSettingsInterface } from './config-interfaces/ConfigurationSettingsInterface'
import { ServerConfigurationInterface } from './config-interfaces/ServerConfigurationInterface'

const numberOfCPUs: number = os.cpus().length;
const allCofigurations: ConfigurationSettingsInterface = Configs.getConfigurations()
const serverConfig: ServerConfigurationInterface = allCofigurations.serverConfig

export const init = async () => {

  try {
    const app: express.Application = express();

    app.use(helmet());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    const port = process.env.PORT || serverConfig.port
    app.use((req, res, next) => {
      res.on('finish', () => {
        const { statusCode, statusMessage } = res;
        if (statusCode) {
          console.log(`${req.socket.remoteAddress}: ${req.method.toUpperCase()} ${req.url} --> ${statusCode}`);
        } else {
          console.log('No statusCode : ', `${req.socket.remoteAddress}: ${req.method.toUpperCase()} ${req.url} --> `);
        }
      });
      next();
    });
    if (process.env.NODE_ENV !== 'test' && cluster.isMaster) {
      for (let i = 0; i < numberOfCPUs; i++) {
        const worker = cluster.fork();
        console.log('worker %s started.', worker.id);
      }
    } else {
      let Route = await component();
      Route.forEach(async (component: any) => {
        console.log(`Component registered ==> ${component}`);
        const TemplateController = await import(`./components/${component}/routes`);
        let controller: any = new TemplateController.default();
        app.use('/', controller.router);
      });
      app.use(
        '/doc',
        swaggerUi.serve,
        swaggerUi.setup(swaggerDocument.Doc, { explorer: true }),
      );
      app.listen(port, () => {
        console.log('App listening on port : ', port);
      });

    }
  } catch (err) {
    console.log('Error starting server: ', err);
    throw err;
  }
};
