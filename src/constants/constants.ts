/*
 * Created on Wed May 06 2020 1:16:41 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 Tech Mahindra Limited
 */
/* import Config from 'config'

exports.HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

exports.MS_URLS = {
  RULEENGINE_BASE_URL: `${Config.get('ruleengine_env.protocol')}://${Config.get('ruleengine_env.host')}${(Config.get('ruleengine_env.port') !== '') ? ':' : ''}${Config.get('ruleengine_env.port')}${(Config.get('ruleengine_env.directory') !== '') ? (`/${Config.get('ruleengine_env.directory')}`) : ''}`,
};

exports.CONTENT_TYPE = {
  JSON: 'application/json',
  FORM: 'application/x-www-form-urlencoded',
  FORM_DATA: 'multipart/form-data',
  XML: 'application/xml',
  TEXT_XML: 'text/xml',
};
 */