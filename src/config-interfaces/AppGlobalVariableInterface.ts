import { ServerConfigurationInterface } from './ServerConfigurationInterface';
import { SwaggerConfigurationInterface } from './SwaggerConfigurationInterface';

export interface AppGlobalVariableInterface {
    mongoInstance?: any;
    serverConfig?: ServerConfigurationInterface;
    swaggerConfig?:SwaggerConfigurationInterface;
}
