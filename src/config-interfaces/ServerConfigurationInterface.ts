
export interface ServerConfigurationInterface {    
    port: number;
    host: string;
}
