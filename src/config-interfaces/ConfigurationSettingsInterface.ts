import { MongoConfigurationInterface } from './MongoConfigurationInterface'
import { SwaggerConfigurationInterface } from './SwaggerConfigurationInterface'
import { ServerConfigurationInterface } from './ServerConfigurationInterface'

export interface ConfigurationSettingsInterface {
    mongoConfig: MongoConfigurationInterface,
    swaggerConfig: SwaggerConfigurationInterface,
    serverConfig: ServerConfigurationInterface
}
