
import { readdirSync } from 'fs';

const component = () => {
  var param: any = [];
  readdirSync(__dirname, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => param.push(dirent.name)
  );

  return param;
}

export default component;
