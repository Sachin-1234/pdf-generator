
import * as express from 'express';
import Validation from './validations/templates.validations';
import * as validator from 'express-joi-validation'

import TemplatesFactory from '../../components/templates/factory/templates.factory';

class TemplateController {
  public router = express.Router();
  private templatesFactory = new TemplatesFactory();
  private validate = validator.createValidator({});

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes() {
    this.router.post('/register',
      this.validate.headers(Validation.registerTemplate.headers),
      this.validate.body(Validation.registerTemplate.body),
      this.templatesFactory.registerTemplates);

    this.router.post('/generate-pdf/:id',
      this.validate.headers(Validation.generatePdf.headers),
      this.validate.body(Validation.generatePdf.body),
      this.validate.query(Validation.generatePdf.query),
      this.validate.params(Validation.generatePdf.params),
      this.templatesFactory.generatePdf);
  }
}

export default TemplateController