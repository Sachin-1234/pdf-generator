
import * as Joi from 'joi';

/**
 * Input JSON validation
 *
 * @type {{}}
 */
const templateValidation = (() => ({
  registerTemplate: {
    headers: Joi.object({ authorization: Joi.string().required() }).options({ allowUnknown: true }),
    body: Joi.object({
      template_file: Joi.any()
        .meta({ swaggerType: 'file' })
        .description('file'),
    }),
  },
  generatePdf: {
    headers: Joi.object({ authorization: Joi.string().required() }).options({ allowUnknown: true }),
    body:Joi.object().required(),
    query: Joi.object().required(),
    params: Joi.object({
      id: Joi.string().required(),
    }),
  }
}))();

export default templateValidation
