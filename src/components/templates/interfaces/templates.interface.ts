/*
 * Created on Fri Jul 03 2020 3:31:39 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 iauro system pvt ltd
 */


// eslint-disable-next-line import/no-unresolved
import { TemplatesModel } from './../models/templates'
      
const createTemplate = (data:Object) => {
  const newRecord = new TemplatesModel(data);
  return newRecord.save();
};
const createManyTemplates = (data:Object) => TemplatesModel.insertMany(data);

const findOneTemplate = (condition:Object, setValues:Object) => TemplatesModel.findOne(condition, setValues);

const getCount = (condition:Object) => TemplatesModel.countDocuments(condition);

const findTemplates = (condition:Object, selectValues:Object, data:any, sort:number) => TemplatesModel.find(condition, selectValues).skip(((data.page - 1) * data.limit)).limit(data.limit).sort(sort);

const getDistinct = (field:any, condition:Object) => TemplatesModel.distinct(field, condition);

const aggregate = (pipeline:Array<Object>) => TemplatesModel.aggregate(pipeline).allowDiskUse(true);

const getMaxTimeStampRecord = (condition:Object) => TemplatesModel.find(condition, {_id: 0, timestamp: 1}).sort({ timestamp: -1 }).limit(1);


export const TemplatesInterFace = {
  createTemplate,
  createManyTemplates,
  findOneTemplate,
  getCount,
  findTemplates,
  getDistinct,
  aggregate,
  getMaxTimeStampRecord
};
