/*
 * Created on Fri Jul 03 2020 3:31:39 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 iauro system pvt ltd
 */


// eslint-disable-next-line import/no-unresolved
import { GeneratedFiles }  from './../models/genarated_files'
      
const createRecord = (data:Object) => {
  const newRecord = new GeneratedFiles(data);
  return newRecord.save();
};
const createManyRecords = (data:Object) => GeneratedFiles.insertMany(data);

const findOneRecord = (condition:Object, setValues:Object) => GeneratedFiles.findOne(condition, setValues);

const getCount = (condition:Object) => GeneratedFiles.countDocuments(condition);

const findRecords = (condition:Object, selectValues:Object, data:any, sort:number) => GeneratedFiles.find(condition, selectValues).skip(((data.page - 1) * data.limit)).limit(data.limit).sort(sort);

const getDistinct = (field:any, condition:Object) => GeneratedFiles.distinct(field, condition);

const aggregate = (pipeline:Array<Object>) => GeneratedFiles.aggregate(pipeline).allowDiskUse(true);

const getMaxTimeStampRecord = (condition:Object) => GeneratedFiles.find(condition, {_id: 0, timestamp: 1}).sort({ timestamp: -1 }).limit(1);


export const GeneratedFilesInterface = {
  createRecord,
  createManyRecords,
  findOneRecord,
  getCount,
  findRecords,
  getDistinct,
  aggregate,
  getMaxTimeStampRecord
};
