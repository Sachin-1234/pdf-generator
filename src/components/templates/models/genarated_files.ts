import * as mongoose from 'mongoose';

const { Schema } = mongoose;

const generatedFilesRootSchema = new Schema({
  file_path: String,
  template_id: String,
  placeholder_json_file: String
},
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

export const GeneratedFiles = mongoose.model('generated_files', generatedFilesRootSchema, 'generated_files');