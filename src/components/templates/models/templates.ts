import * as mongoose from 'mongoose';

const { Schema } = mongoose;

const templatesRootSchema = new Schema({
  template_file_path: String,
  placeholder_json: Object,
  template_file_name: String
},
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

export const TemplatesModel = mongoose.model('templates', templatesRootSchema, 'templates');
