
import * as fs from 'fs'
import * as puppeteer from 'puppeteer'
import * as hb from 'handlebars'
import { TemplatesInterFace } from '../interfaces/templates.interface'
import { GeneratedFilesInterface } from '../interfaces/generated_files.interface'
import * as Formidable from 'formidable'
import { Request, Response } from 'express'

export default class TemplatesFactory {

  /**
   * registers the invoice template
   * @param req
   * @param res
   */
  registerTemplates = async (req: Request, res: Response) => {
    try {
      const form: any = new Formidable.IncomingForm();

      //Formidable uploads to operating systems tmp dir by default
      form.uploadDir = "./templates";
      form.keepExtensions = true;

      form.parse(req, async (err: any, fields: any, files: any) => {
        if (err)
          throw err;
        res.writeHead(200, { 'content-type': 'text/plain' });
        res.write('template registered:\n\n');
        const fileData = {
          template_file_path: files.template_file.path,
          placeholder_json: JSON.parse(fields.placeholder_json),
          template_file_name: files.template_file.name
        };
        await TemplatesInterFace.createTemplate(fileData);
        res.end();
      });
    } catch (e) {
      console.error('ERROR => ', e);
      res.status(400).json({ error: 'An error occured!' });
    }
  };

  compareJson(obj1: Object, obj2: Object) {
    let tree1 = this.getKeys(obj1).sort();
    let tree2 = this.getKeys(obj2).sort();

    if (tree1.length !== tree2.length)
      return false;

    let mismatch = tree1.find((x: String, idx: number) => tree2[idx] !== x);
    console.log('the ismatch is:', mismatch);
    return !mismatch;
  }

  getKeys(obj: Object) {
    return this.recursiveKeys(obj, [], []);
  }

  recursiveKeys(obj: any, result: Array<String>, todo: Array<any>, root = ''): String[] {
    Object.keys(obj).forEach(key => {
      if (typeof obj[key] === 'object') {
        result.push(root + key);
        todo.push({ obj: obj[key], root: root + key + '.' });
      } else {
        result.push(root + key);
      }
    });

    if (todo.length > 0) {
      let todoItem = todo.pop();
      return this.recursiveKeys(todoItem.obj, result, todo, todoItem.root);
    } else {
      return result;
    }
  }
  /**
   * generate the pdf for template id stores the generated pdf in generated_files folder and
   * downloads that generated pdf file
   * @param req
   * @param res
   */
  generatePdf = async (req: Request, res: Response) => {
    try {
      // getting templates details from db
      const templateDetails: any = await TemplatesInterFace.findOneTemplate({ _id: req.params.id }, {})
      // reading template file
      const html: String = fs.readFileSync(templateDetails.template_file_path, 'utf-8');
      // reading placeholder file that is registered with template 
      const template_placeholder: Object = templateDetails.placeholder_json;
      console.log('the template placeholder is:', template_placeholder);
      // reading input placeholder json file
      const input_json: Object = req.body;
      console.log('the input json is:', input_json);
      const isSame = this.compareJson(template_placeholder, input_json);
      console.log('is same:', isSame);
      if (!isSame) {
        return res.status(400).json({ message: "invalid input placeholder_json for this template...!!!!!" });
      }

      const template = hb.compile(html, { strict: true });
      const result = template(input_json);
      // we are using headless mode
      const browser = await puppeteer.launch({ignoreDefaultArgs: ['--disable-extensions'],args: ['--no-sandbox']});
      const page = await browser.newPage();
      // We set the page content as the generated html by handlebars
      await page.setContent(result)
      const templateFileName = templateDetails.template_file_name.split('.')[0];
      // We use pdf function to generate the pdf in the generated_files folder as this file.
      const pdfFilePath = './' + 'generated_files' + '/' + templateFileName + '.pdf';
      const margin = JSON.parse((req.query.margin) as string);
      await page.pdf({ path: pdfFilePath, format: 'a4' , margin:margin});
      await browser.close();
      console.log('PDF Generated');
      const generatedfileData = {
        file_path: pdfFilePath,
        template_id: templateDetails._id,
        placeholder_json: req.body.placeholder_json
      }
      await GeneratedFilesInterface.createRecord(generatedfileData);
      res.download(pdfFilePath);
    } catch (err) {
      console.log(err);
      res.status(400).json({ error: 'An error occured!' });
    }
  };
}