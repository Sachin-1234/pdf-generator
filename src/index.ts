/* eslint-disable import/no-unresolved */
//process.env.NODE_ENV = 'local';
import * as app_module_path from 'app-module-path'
import * as Server from './server'
import { ConfigurationSettingsInterface } from './config-interfaces/ConfigurationSettingsInterface'
import { MongoConfigurationInterface } from './config-interfaces/MongoConfigurationInterface'
import { AppGlobalVariableInterface } from './config-interfaces/AppGlobalVariableInterface'
import * as MongoDbConnection from "./configurations/mongo-connection"
import * as Configs from './configurations'

app_module_path.addPath(__dirname);
console.log(`Running environment ==> ${process.env.NODE_ENV}`);

// Catch unhandling unexpected exceptions
process.on('uncaughtException', (error) => {
  console.log(`uncaughtException ==> ${error.message}`);
});

// Catch unhandling rejected promises
process.on('unhandledRejection', (reason) => {
  console.log(`unhandledRejection ==> ${reason}`);
});

const allConfigurations: ConfigurationSettingsInterface = Configs.getConfigurations();


let mongoConfig: MongoConfigurationInterface = allConfigurations.mongoConfig;

if (process.env.mongoConnectionString)
  mongoConfig.connectionString = process.env.mongoConnectionString;

let mongoDbInstance = MongoDbConnection.init(mongoConfig);
(async () => {
  try {
    // Start the node server
    Server.init();
  } catch (err) {
    console.log('Error in the node server ==> ', err);
  }
})();
