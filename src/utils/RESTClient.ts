/* eslint-disable import/no-unresolved */

/*
 * Created on Wed May 06 2020 1:16:13 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 Tech Mahindra Limited
 */


const CallAPI = require('src/classes/RESTClient');


module.exports = (options:object) => new Promise((resolve, reject) => {
  const data = new CallAPI(options);
  data.makeRequest(resolve, reject);
});
