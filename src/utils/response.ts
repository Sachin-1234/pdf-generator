/*
 * Created on Wed May 06 2020 1:16:21 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 Tech Mahindra Limited
 */

module.exports = class Response {
  static sendResponse(isSuccess:any, result:any, message:any, statusCode:any) {
    return {
      is_success: isSuccess,
      result,
      message,
      status_code: statusCode,
    };
  }
};
