/*
 * Created on Wed May 06 2020 1:16:34 PM
 *
 * Created by sudhir.raut@iauro.com
 * Copyright (c) 2020 Tech Mahindra Limited
 */


/**
  * Function to handle errors in promises
  */
module.exports = (promise:any) => promise.then((data:any) => [null, data])
  .catch((err:any) => [err]);
