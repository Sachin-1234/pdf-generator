/**
 * Catch Errors Handler
 * Instead of using try{} catch(e) {} in each controller, we wrap the function in
 * catchErrors(), catch any errors they throw, and pass it along to our express middleware with next().
 */

export const catchErrors = (fn:any) => {
  return function (request:any, response:any, next:any) {
    return fn(request, response, next).catch((e:any) => {
      if (e.response) {
        e.status = e.response.status
      }
      next(e)
    })
  }
}
//export default catchErrors