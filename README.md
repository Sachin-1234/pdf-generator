# IP Express sample code skeleton

This is the sample project created as an output reference for the internal project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Environment Variables

These are the environment variables to be provided while docker run.

```
NODE_ENV


```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
npm i
```


```
nde index.js
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system





## Authors

* **Sudhir Raut** - *Initial work* - [Github](https://github.com/sudhirraut)


## License

This project is licensed under the TechMahindra License - see the [LICENSE.md](LICENSE.md) file for details
